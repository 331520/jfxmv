package com.melonsoft.mavenfx;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class FXMLController implements Initializable {

    @FXML
    private Label label;
    
    @FXML private javafx.scene.control.Button button;
    
    @FXML private void handleButtonAction(ActionEvent event) throws IOException {
        //Close current
        Stage stage = (Stage) button.getScene().getWindow();
        // do what you have to do
        stage.close();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Window2.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Другая форма");
        stage.setScene(new Scene(root1));
        stage.show();
    }

    @FXML
    private void handleButtonActionAnotherForm(ActionEvent event) throws IOException {
        System.out.println("You clicked me!");

        /*
            //Close current
            Stage stage = (Stage) button.getScene().getWindow();
            // do what you have to do
            stage.close();
         */
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/anotherForm.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Вообще другая форма");
        stage.setScene(new Scene(root1));
        stage.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
}
